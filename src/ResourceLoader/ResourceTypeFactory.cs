﻿using Coscine.Configuration;
using Coscine.ResourceTypeBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Coscine.ResourceLoader
{
    public class ResourceTypeFactory
    {
        public static string FolderLocationKey { get; } = "coscine/local/resourcetypes/folder";

        public static ResourceTypeDefinition CreateResourceTypeObject(string type, IConfiguration coscineConfiguration, Assembly assembly = null)
        {
            var resourceConfiguration = new ResourceConfiguration.ResourceConfiguration();
            if (!resourceConfiguration.IsValidResource(type))
            {
                return null;
            }

            var resourceTypeObject = resourceConfiguration.GetResourceType(type);

            if (resourceTypeObject.Status == "disabled")
            {
                return null;
            }

            if(assembly == null)
            {
                var basePath = coscineConfiguration.GetStringAndWait(FolderLocationKey);
                var pathToDll = basePath + resourceTypeObject.Path;

                // dll not found
                if (assembly == null && !File.Exists(pathToDll))
                {
                    return null;
                }
                assembly = Assembly.LoadFile(pathToDll);
            }

            object[] args = { type, coscineConfiguration, resourceTypeObject };

            foreach (var assemblyType in assembly.GetExportedTypes())
            {
                if (typeof(ResourceTypeDefinition).IsAssignableFrom(assemblyType) && assemblyType.Name == resourceTypeObject.ClassName)
                {
                   return (ResourceTypeDefinition)Activator.CreateInstance(assemblyType, args);
                }
            }

            return null;            
        }

        public static List<string> GetAvailableResourceTypes()
        {
            return new ResourceConfiguration.ResourceConfiguration().GetAvailableResourceTypes();
        }

        public static List<string> GetResourceTypesByStatus(params string[] stati)
        {
            var resourceTypes = new List<string>();
            foreach(string status in stati)
            {
                resourceTypes.AddRange(new ResourceConfiguration.ResourceConfiguration().GetResourceTypesByStatus(status));
            }
            return resourceTypes;
        }
    }
}
