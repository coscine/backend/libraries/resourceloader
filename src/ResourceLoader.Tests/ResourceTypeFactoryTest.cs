﻿using Coscine.Configuration;
using NUnit.Framework;
using System.Linq;

namespace Coscine.ResourceLoader.Tests
{
    [TestFixture]
    public class ResourceTypeFactoryTest
    {
        [OneTimeSetUp]
        public void Setup()
        {

        }

        [OneTimeTearDown]
        public void End()
        {
        }

        [Test]
        public void TestResourceTypeFactoryGetAvailableResources()
        {
            Assert.NotNull(ResourceTypeFactory.GetAvailableResourceTypes());
        }
    }
}
